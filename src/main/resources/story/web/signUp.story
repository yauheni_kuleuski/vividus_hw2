Description: Simple story to check the page availability;

Scenario: Open application under test
Given I am on the main application page

Scenario: Verify page title
Then the page title is equal to 'Trello'

Scenario: Register a new user
When I click on an element by the xpath '//a[@data-analytics-button="whiteSignupHeroButton"]'
Then an element by the xpath '//div[@id="signup-form"]' exists

When I enter <email> in a field by the xpath '//form[@id="signup"]/input[@id="email"]'
When I click on an element by the xpath '//form[@id="signup"]/input[@id="signup-submit"]'
When I wait until an element with the xpath '//form[@id="form-sign-up"]' appears
When I enter <displayName> in a field by the xpath '//form[@id="form-sign-up"]//input[@id="displayName"]'
When I enter <password> in a field by the xpath '//form[@id="signup"]/input[@id="password"]'
When I click on an element by the xpath '//form[@id="form-sign-up"]//button[@id="signup-submit"]'
Then an element by the xpath '//div[@id="content"]//img[contains(@alt, "Trello")]' exists
Examples:
|email                |displayName |password |
|test1_email@gmail.com|test_name1  |password1|
|test2_email@gmail.com|test_name2  |password2|